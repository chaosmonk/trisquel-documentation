# Installing and Removing Software from the Repository

## Install or Remove Applications

To install or remove an application, [launch](navigate-desktop.md#launching-programs) `Add/Remove Applications`.

Browse installed and available applications. Select applications that you would like to install.

![](app-install-select.png)

Deselect applications that you would like to remove.

![](app-install-deselect.png)

When you are finished, click `Apply Changes`.

![](app-install-apply-changes.png)

Review the applications to be installed and removed. If you are satisfied, click `Apply` and enter your password.

![](app-install-apply.png)

Double click a newly installed application to launch it immediately.

![](app-install-launch.png)

## Install or Remove Packages

Software packages that are not applications, such as utilities, drivers, or libraries, do not appear in `Add/Remove Applications`. However, all software packages can be installed or removed using Synaptic Package Manager.

[Launch](navigate-desktop.md#launching-programs) `Synaptic Package Manager` and enter your password.

Right click and `Mark for installation` packages that you would like to install.

![](synaptic-mark-for-installation.png)

Right click and `Mark for removal` packages you wish to remove.

![](synaptic-mark-for-removal.png)

The installation or removal of some packages may require other packages to be installed or removed. In this case, click `Mark` to confirm.

![](synaptic-mark-to-confirm.png)

When you are finished, click `Apply`.

![](synaptic-apply-changes.png)

Review the list of packages to be installed and removed. If you are satisfied, click `Apply`.

![](synaptic-apply.png)

## Install or Remove Packages (Terminal)

See [Introduction to the Command Line](command-line.md)

Search for packages in the repository with `apt search [search query]`.

```
$ apt search alex alleg
Sorting... Done
Full Text Search... Done
alex4/flidas 1.1-6 amd64
  Alex the Allegator 4 - a retro platform game

alex4-data/flidas,flidas 1.1-6 all
  Alex the Allegator 4 - game data
```

Install a package with `sudo apt install [package name]`.

```
$ sudo apt install alex4
```

Remove a package with `sudo apt remove [package name]`.

```
$ sudo apt remove alex4
```
