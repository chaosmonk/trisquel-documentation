Introduction to Package Management
==================================

Repositories and the Package Manager
------------------------------------

Software installation works differently in GNU/Linux from how it does in Windows and macOS. The preferred method of installing software is not from a CD or an installer found on the Internet, but from your GNU/Linux distribution's repositories.

Trisquel's repositories contain a large collection of free software. Most software that you will need can be [installed from the repository](install-remove-software.md) using the package manager.

The package manager is a tool which automatically handles various software management tasks:

- Searching the repository for available software
- Detecting and installing dependencies of software packages to be installed
- Keeping track of which packages are installed, so that they can be easily removed
- Checking for available bug fixes, security updates, and newer versions of software packages

Long-Term Stable Release Cycle
------------------------------

Trisquel is a long-term stable (LTS) distribution. This means that software packages are based on versions from a fixed snapshot date. If a new version of a program is released after the snapshot date, Trisquel does not upgrade to the newer version. Instead, any bug fixes or security fixes are backported from the new version into Trisquel's version.

The reason for this is stability. With new versions of a program come the risk of new bugs. The LTS approach ensures that you can safely [update your system](update-upgrade.md) for bug and security fixes, without introducing new bugs or security flaws. When a new version of Trisquel is released, you have the option to upgrade your entire system to the new snapshot date.

Sometimes, a newer version of a package might be available in Trisquel's backports repository. The backports repository is disabled by default so that users do not experience unwanted changes to their system. However, you have the option of [enabling the backports repository](backports-repository.md).

Some GNU/Linux distributions use a rolling release cycle instead of an LTS release cycle. With a rolling distribution, the repository always provides the newest version of each program. The advantage is that users always benefit from the newest features. The disadvantage is that some updates might be unwanted, or even break your system. [Parabola GNU/Linux](https://www.parabola.nu/) is a 100% free software distribution with a rolling release cycle.

Software From Outside of the Repositories
-----------------------------------------

It is also possible to install software that is not in the repository, or a version of a package that is newer than the version in the repository. There are several ways to [install software from third parties](third-party-software.md), but they come with some disadvantages.

- Whereas all of the software in Trisquel's repositories is 100% free software, when installing software from a third party you may need to investigate the software yourself to make sure that it is free software.
- The software in Trisquel's repositories has been compiled from the source code on Trisquel's servers. This ensures that the compiled binaries have not been tampered with. Trisquel cannot ensure that software from third parties does not contain malware. Avoid installing software from untrusted sources.
- The package manager cannot provide automatic bug fixes and security updates for software installed from third-party sources.
