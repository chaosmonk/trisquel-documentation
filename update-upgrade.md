# Updating/Upgrading your System

## Manual Updates

To check for updates manually, [launch](navigate-desktop.md#launching-programs) `Software Updater`.

![](checking-for-updates.png)

## Automatic Updates

To automatically check for updates, [launch](navigate-desktop.md#launching-programs) `Software & Updates`. 

![](software-and-updates-updates.png)

## The Backports Repository

![](enable-backports.png)

## Upgrading to a new version of Trisquel

## Install a Hardware Enablement Stack

As a [long-term stable](package-management.md#long-term-stable-release-cycle) distribution, each version of Trisquel provides security updates and bug fixes to software packages in its repositories, rather than upgrade packages to newer versions. As a result, some newer hardware may not be properly supported by the default kernel and graphics stack.

Trisquel provides a Hardware Enablement (HWE) stack with a newer kernel and graphics stack than Trisque's default. If your hardware is working properly, there is generally no reason to install the HWE stack. However, installing the HWE stack may fix problems with newer hardware.

To install the HWE stack, [install](install-remove-software.md#install-or-remove-packages) the packages `linux-generic-hwe-[trisquel version]` and `xserver-xorg-hwe-[trisquel version]`, where `[trisquel version]` is the version of your Trisquel release. For example, if you are using Trisquel 9.0, install `linux-generic-hwe-9.0` and `xserver-xorg-hwe-9.0`.
