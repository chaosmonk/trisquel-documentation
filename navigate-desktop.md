# Navigating the Desktop

## Launching Programs

Browse and launch programs from your desktop environment's main menu.

| Trisquel                    | Trisquel Mini                    |
| --------------------------- | -------------------------------- |
| ![](main-menu-trisquel.png) | ![](main-menu-trisquel-mini.png) |

If you already know the name of the program you want to launch, launch it from your desktop environment's search bar.

| Trisquel                     | Trisquel Mini                 |
| ---------------------------- | ----------------------------- |
| ![](search-bar-trisquel.png) | (n/a)                         |

## File Management
